jQuery(document).ready(function(){
	
	jQuery(".scrolltop").click(function(){
		jQuery("html").animate({'scrollTop' : '0'}, 3000);	
		return false;
	});
	
	jQuery(window).scroll(function(){
		
		var utd = jQuery(window).scrollTop();
		
		if(utd > 1000) {
			jQuery('.scrolltop a').show(
				
			)
		}else {
			jQuery('.scrolltop a').removeAttr('style')
		}
	});
	
	// wow js
	new WOW().init();
	
	
	// Feature Video
	var vid = document.getElementById("bgvid"),
		pauseButton = document.getElementById("vidpause");
	
	pauseButton.addEventListener("click", function(){
		if (vid.paused) {
			vid.play();
		} else {
			vid.pause();
		}
	});
	
	// vimo video
	jQuery(".container").fitVids();
	
	// Owl carousel
	jQuery(".app-demo").owlCarousel({
		'autoPlay' : 3000,
		'items' : 4,
		'itemsTablet' : [768, 2],
		'itemsMobile' : [360, 1],
		'stopOnHover' : true,
	});
	
	jQuery(".client-comment").owlCarousel({
		'autoPlay' : 3000,
		'items' : 1,
		'itemsTablet' : [768, 1],
		'itemsMobile' : [360, 1],
		'stopOnHover' : true,
	});
	
	// Sticky Menu
	jQuery(".top-area").sticky({topSpacing: 0});
	
	// jQuery smooth-Scroll: 
    jQuery('li.smooth-menu a').bind('click', function(event){
        var $anchor = $(this);
        var headerH = '68';
        jQuery('html, body').stop().animate({
            scrollTop : $($anchor.attr('href')).offset().top - headerH + "px"
            }, 1200, 'easeInOutExpo');
            event.preventDefault();
    });
	
	
	

});